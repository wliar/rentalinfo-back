# Assignment 1 - Agile Software Practice.

Name: Junyu Liang

## Overview.

Rental Information System. It can manage the rental information and house owner information.

## API endpoints.

 + GET /renting - Get all rental information.
 + GET /rentingHouseOwner - Get all house owners' information.
 + GET /rentingId/:id - Get one piece of rental information by object id.
 + GET /rentingHouseOwnerName/:HouseOwnerName - Get one piece of rental information by HouseOwnerName.
 + POST /renting - Add one piece of new rental information.
 + PUT /renting/:id - Update one piece of rental information by object id.
 + PUT /rentingHouseOwner/:HouseOwnerName - Update one piece of house owner's information.
 + DELETE /renting/:id - Delete one piece of rental information by object id.
 + DELETE /rentingHouseOwner/:HouseOwnerName - Delete one piece of house owner's information by HouseOwnerName.

## Data model.

. . . . Describe the structure of the database being used by your API. An image (see example below) or JSON represerntation is acceptable . . . . 

![][datamodel]



## Sample Test execution.


~~~
  
  HouseOwner
    GET /rentingHouseOwner 200 77.343 ms - 214
      √ should GET all the house owners' information (98ms)
    PUT /rentingHouseOwner/:HouseOwnerName
      when the HouseOwnerName is valid
PUT /rentingHouseOwner/OvenJu 200 126.507 ms - 35
        √ should return a message and the update the matching rental information (130ms)
      when the  HouseOwnerName is invalid
PUT /rentingHouseOwner/12345 200 33.197 ms - 35
        √ should return a message for invalid HouseOwnerName
    DELETE /rentingHouseOwner/:HouseOwnerName
      when the HouseOwnerName is valid
DELETE /rentingHouseOwner/OvenJu 200 48.101 ms - 35
        √ should return a confirmation message and the deleted house owner information (54ms)
GET /rentingHouseOwner 200 46.115 ms - 110
      when the HouseOwnerName is invalid
DELETE /rentingHouseOwner/12345 200 49.426 ms - 52
        √ should return an error message (53ms)

  RentalInformation
    GET /renting
GET /renting 200 2.489 ms - 219
      √ should GET all the rental information
    GET /rentingId/:id
      when the id is valid
{ id: '5dc1958a47061f2a6cb5e805' }
GET /rentingId/5dc1958a47061f2a6cb5e805 200 5.250 ms - 107
        √ should return the matching rental information
      when the id is invalid
{ id: '1234' }
GET /rentingId/1234 200 2.155 ms - 240
        √ should return the NOT found message
    GET /rentingHouseOwnerName/:HouseOwnerName
      when the HouseOwnerName is valid
GET /rentingHouseOwnerName/OvenJu 200 5.052 ms - 107
        √ should return the matching rental information
      when the id is invalid
GET /rentingHouseOwnerName/1234 200 2.268 ms - 2
        √ should return the NOT found message
    POST /renting
POST /renting 200 2.240 ms - 150
      √ should return confirmation message and update datastore
{ id: '5dc1958a47061f2a6cb5e80e' }
GET /rentingId/5dc1958a47061f2a6cb5e80e 200 2.596 ms - 111
    PUT /renting/:id
      when the id is valid
PUT /renting/5dc1958a47061f2a6cb5e810 200 56.936 ms - 36
        √ should return a message and the update the matching rental information (62ms)
{ id: '5dc1958a47061f2a6cb5e810' }
GET /rentingId/5dc1958a47061f2a6cb5e810 200 3.557 ms - 111
      when the id is invalid
PUT /renting/1234567 200 0.526 ms - 248
        √ should return a message for invalid id
    DELETE /renting/:id
      when id is valid
DELETE /renting/5dc1958a47061f2a6cb5e814 200 4.434 ms - 35
        √ should return a confirmation message and the deleted rental information
GET /renting 200 1.793 ms - 113
      when id is invalid
DELETE /renting/1234567 200 0.692 ms - 251
        √ should return an error message


  15 passing (4s)

~~~

[ Markdown Tip: By wrapping the test results in fences (~~~), GitHub will display it in a 'box' and preserve any formatting.]




[datamodel]: ./img/models.PNG

[![pipeline status](https://gitlab.com/wliar/rentalinfo-back/badges/master/pipeline.svg)](https://gitlab.com/oconnordiarmuid/donations-CICD/commits/master)

[![coverage report](https://gitlab.com/wliar/rentalinfo-back/badges/master/coverage.svg)](https://gitlab.com/oconnordiarmuid/donations-CICD/badges/master/coverage.svg?job=coverage)

## Donations Web API.
