var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors')
let mongoose = require('mongoose');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

const renting = require("./routes/renting");


// var mongodbUri = 'mongodb+srv://William:1231231@wit-rentalinfo-cluster-wt24m.mongodb.net/test?retryWrites=true&w=majority';

var mongodbUri = `${process.env.MONGO_URI}${process.env.MONGO_DB}`
mongoose.connect(mongodbUri, { dbName: 'rentalinfodb' });
let db = mongoose.connection;
db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});
db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ]');
});


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.get('/renting', renting.findAllInfo);
app.get('/rentingHouseOwner', renting.findAllOwner);
app.get('/rentingId/:id', renting.findOneId);
app.get('/rentingHouseOwnerName/:HouseOwnerName', renting.findOneHouseOwnerName);

app.post('/renting', renting.addInfo);
app.put('/renting/:id', renting.modifyUpHouses);
app.put('/rentingHouseOwner/:HouseOwnerName', renting.updateHouseOwner);
app.delete('/rentingHouseOwner/:HouseOwnerName', renting.deleteOwner);
app.delete('/renting/:id', renting.deleteInfo);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
