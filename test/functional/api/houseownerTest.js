const chai = require("chai");
chai.use(require('chai-things'));
let chaiHttp = require('chai-http');
const expect = chai.expect;
const request = require("supertest");
const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer;
const HouseOwner = require("../../../models/HouseOwner");
const mongoose = require("mongoose");

chai.use(chaiHttp);
const _ = require("lodash");
let server;
let mongod;
let db, validID;

describe("HouseOwner", () => {
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: "./test/database",
                    dbName: process.env.MONGO_DB // by default generate random dbName
                }
            });
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            // await mongod.getConnectionString();

            mongoose.connect(process.env.MONGO_URI+process.env.MONGO_DB, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
            server = require("../../../bin/www");
            db = mongoose.connection;
        } catch (error) {
            console.log(error);
        }
    });

    after(async () => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });

    beforeEach(async () => {
        try {
            await HouseOwner.deleteMany({});
            let houseowner = new HouseOwner();
            houseowner.houseOwnerName = "StephenZhao";
            houseowner.telephone = "12345678";
            houseowner.email = "SZ@wit.ie";
            await houseowner.save();
            houseowner = new HouseOwner();
            houseowner.houseOwnerName = "OvenJu";
            houseowner.telephone = "12345679";
            houseowner.email = "OJ@wit.ie";
            await houseowner.save();
            houseowner = await HouseOwner.findOne({houseOwnerName: "OvenJu"});
            validID = houseowner._id;
        } catch (error) {
            console.log(error);
        }
    });


    describe("GET /rentingHouseOwner", () => {
        it("should GET all the house owners' information", done => {
            request(server)
                .get("/rentingHouseOwner")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    try {
                        expect(res.body).to.be.a("array");
                        expect(res.body.length).to.equal(2);
                        let result = _.map(res.body, houseowner => {
                            return {
                                houseOwnerName: houseowner.houseOwnerName,
                                telephone: houseowner.telephone,
                                email: houseowner.email
                            };
                        });
                        expect(result).to.deep.include({
                            houseOwnerName: "StephenZhao",
                            telephone: "12345678",
                            email: "SZ@wit.ie"
                        });
                        expect(result).to.deep.include({
                            houseOwnerName: "OvenJu",
                            telephone: "12345679",
                            email: "OJ@wit.ie"
                        });
                        done();
                    } catch (e) {
                        done(e);
                    }
                });
        });
    });

    describe("PUT /rentingHouseOwner/:HouseOwnerName", () => {
        describe("when the HouseOwnerName is valid", () => {
            it("should return a message and the update the matching rental information", () => {
                const houseowner = {
                    houseOwnerName: "MengQian",
                    telephone: "12345670",
                    email: "MQ@wit.ie"
                };
                return request(server)
                    .put("/rentingHouseOwner/OvenJu")
                    .send(houseowner)
                    .expect(200)
                    .then(resp => {
                        expect(resp.body).to.include({
                            message: "Updated Successfully!"
                        });
                    });
            });
        });
    });

    describe('DELETE /rentingHouseOwner/:HouseOwnerName', function () {
        describe('when the HouseOwnerName is valid', function () {
            it('should return a confirmation message and the deleted house owner information', function(done) {
                chai.request(server)
                    .delete("/rentingHouseOwner/OvenJu")
                    .end( (err, res) => {
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('message','Deleted Successfully!' ) ;
                        done();
                    });
            });
            after(function  (done) {
                chai.request(server)
                    .get('/rentingHouseOwner')
                    .end(function(err, res) {
                        expect(res).to.have.status(200);
                        expect(res.body).be.be.a('array');
                        let result = _.map(res.body, function (houseowner) {
                            return { houseOwnerName: houseowner.houseOwnerName,
                                telephone: houseowner.telephone,
                                email: houseowner.email};
                        }  );
                        expect(result).to.not.include( { houseOwnerName: "OvenJu", telephone: "12345679", email: "OJ@wit.ie" } );
                        done();
                    });
            });
        });
    });
});
