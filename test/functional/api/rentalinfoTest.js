const chai = require("chai");
chai.use(require('chai-things'));
let chaiHttp = require('chai-http');
const expect = chai.expect;
const request = require("supertest");
const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer;
const Information = require("../../../models/RentalInformation");
const mongoose = require("mongoose");

chai.use(chaiHttp);
const _ = require("lodash");
let server;
let mongod;
let db, validID;

describe("RentalInformation", () => {
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: "./test/database",
                    dbName: process.env.MONGO_DB  // by default generate random dbName
                }
            });
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            // await mongod.getConnectionString();

            mongoose.connect(process.env.MONGO_URI+process.env.MONGO_DB, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
            server = require("../../../bin/www");
            db = mongoose.connection;
        } catch (error) {
            console.log(error);
        }
    });

    after(async () => {
        try {
            await db.dropDatabase();
        } catch (error) {
            console.log(error);
        }
    });

    beforeEach(async () => {
        try {
            await Information.deleteMany({});
            let information = new Information();
            information.number = "1";
            information.price = "200";
            information.address = "LackenWood";
            information.houseOwner = "StephenZhao";
            await information.save();
            information = new Information();
            information.number = "2";
            information.price = "300";
            information.address = "Riverwalk";
            information.houseOwner = "OvenJu";
            await information.save();
            information = await Information.findOne({houseOwner: "OvenJu"});
            validID = information._id;
        } catch (error) {
            console.log(error);
        }
    });


    describe("GET /renting", () => {
        it("should GET all the rental information", done => {
            request(server)
                .get("/renting")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    try {
                        expect(res.body).to.be.a("array");
                        expect(res.body.length).to.equal(2);
                        let result = _.map(res.body, information => {
                            return {
                                number: information.number,
                                price: information.price,
                                address: information.address,
                                houseOwner: information.houseOwner
                            };
                        });
                        expect(result).to.deep.include({
                            number: "1",
                            price: "200",
                            address: "LackenWood",
                            houseOwner: "StephenZhao"
                        });
                        expect(result).to.deep.include({
                            number: "2",
                            price: "300",
                            address: "Riverwalk",
                            houseOwner: "OvenJu"
                        });
                        done();
                    } catch (e) {
                        done(e);
                    }
                });
        });
    });

    describe("GET /rentingId/:id", () => {
        describe("when the id is valid", () => {
            it("should return the matching rental information", done => {
                request(server)
                    .get(`/rentingId/${validID}`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body).to.have.property("number", "2");
                        expect(res.body).to.have.property("price", "300");
                        expect(res.body).to.have.property("address", "Riverwalk");
                        expect(res.body).to.have.property("houseOwner", "OvenJu");
                        done(err);
                    });
            });
        });
        describe("when the id is invalid", () => {
            it("should return the NOT found message", done => {
                request(server)
                    .get("/rentingId/1234")
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body.message).equals("Information NOT FOUND!");
                        done(err);
                    });
            });
        });
    });

    describe("GET /rentingHouseOwnerName/:HouseOwnerName", () => {
        describe("when the HouseOwnerName is valid", () => {
            it("should return the matching rental information", done => {
                request(server)
                    .get("/rentingHouseOwnerName/OvenJu")
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body).to.have.property("number", "2");
                        expect(res.body).to.have.property("price", "300");
                        expect(res.body).to.have.property("address", "Riverwalk");
                        expect(res.body).to.have.property("houseOwner", "OvenJu");
                        done(err);
                    });
            });
        });
        describe("when the id is invalid", () => {
            it("should return the NOT found message", done => {
                request(server)
                    .get("/rentingHouseOwnerName/1234")
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body == null);
                        done(err);
                    });
            });
        });
    });

    describe("POST /renting", () => {
        it("should return confirmation message and update datastore", () => {
            const information = {
                number: "3",
                price: "400",
                address: "LismorePark",
                houseOwner: "MengQian"
            };
            return request(server)
                .post("/renting")
                .send(information)
                .expect(200)
                .then(res => {
                    expect(res.body.message).equals("Added Successfully!");
                    validID = res.body.data._id;
                });
        });
        after(() => {
            return request(server)
                .get(`/rentingId/${validID}`)
                .expect(200)
                .then(res => {
                    expect(res.body).to.have.property("number", "3");
                    expect(res.body).to.have.property("price", "400");
                    expect(res.body).to.have.property("address", "LismorePark");
                    expect(res.body).to.have.property("houseOwner", "MengQian");
                });
        });
    });

    describe("PUT /renting/:id", () => {
        describe("when the id is valid", () => {
            it("should return a message and the update the matching rental information", () => {
                const information = {
                    number: "3",
                    price: "400",
                    address: "LismorePark",
                    houseOwner: "MengQian"
                };
                return request(server)
                    .put(`/renting/${validID}`)
                    .send(information)
                    .expect(200)
                    .then(resp => {
                        expect(resp.body).to.include({
                            message: "Modified Successfully!"
                        });
                    });
            });
            after(() => {
                return request(server)
                    .get(`/rentingId/${validID}`)
                    .expect(200)
                    .then(res => {
                        expect(res.body).to.have.property("number", "3");
                        expect(res.body).to.have.property("price", "400");
                        expect(res.body).to.have.property("address", "LismorePark");
                        expect(res.body).to.have.property("houseOwner", "MengQian");
                    });
            });
        });
        describe("when the id is invalid", () => {
            it("should return a message for invalid id", done => {
                request(server)
                    .put("/renting/1234567")
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body.message).equals("Information NOT Found");
                        done(err);
                    });
            });
        });
    });

    describe('DELETE /renting/:id', function () {
        describe('when id is valid', function () {
            it('should return a confirmation message and the deleted rental information', function(done) {
                chai.request(server)
                    .delete(`/renting/${validID}`)
                    .end( (err, res) => {
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('message','Deleted Successfully!' ) ;
                        done();
                    });
            });
            after(function  (done) {
                chai.request(server)
                    .get('/renting')
                    .end(function(err, res) {
                        expect(res).to.have.status(200);
                        expect(res.body).be.be.a('array');
                        let result = _.map(res.body, function (information) {
                            return { number: information.number,
                                price: information.price,
                                address: information.address,
                                houseOwner: information.houseOwner};
                        }  );
                        expect(result).to.not.include( { number: "2", price: "300", address: "Riverwalk", houseOwner: "OvenJu"  } );
                        done();
                    });
            });
        });
        describe('when id is invalid', function () {
            it('should return an error message', function(done) {
                chai.request(server)
                    .delete('/renting/1234567')
                    .end( (err, res) => {
                        expect(res).to.have.status(200);
                        expect(res.body).to.have.property('message','Information NOT DELETED!' ) ;
                        done();
                    });
            });
        });
    });
});

