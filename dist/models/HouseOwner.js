let mongoose = require('mongoose');

let HouseOwner = new mongoose.Schema({
  houseOwnerName: String,
  telephone: {
    type: String,
    default: ''
  },
  email: {
    type: String,
    default: ''
  },
  pass: {
    type: String,
    default: ''
  }
}, {
  versionKey: false
});
module.exports = mongoose.model('HouseOwner', HouseOwner);