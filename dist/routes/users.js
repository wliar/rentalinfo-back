var express = require('express');

var router = express.Router();

let HouseOwner = require('../models/HouseOwner');
/* GET users listing. */


router.post('/signin', function (req, res, next) {
  // res.send('respond with a resource');
  console.log(req.body);
  HouseOwner.findOne({
    houseOwnerName: req.body.username
  }, (err, user) => {
    if (err) return res.status(400).send(err.message); // console.log(user)

    if (user && user.pass === req.body.password) {
      return res.json({
        success: true,
        user
      });
    } else {
      return res.json({
        success: false,
        msg: "Invailed username or password error"
      });
    }
  });
});
router.post('/signup', function (req, res, next) {
  // res.send('respond with a resource');
  //console.log(req.body)
  HouseOwner.findOne({
    houseOwnerName: req.body.username
  }, (err, user) => {
    if (err) return res.status(500).send(err.message);
    console.log(user);

    if (!user) {
      let user = new HouseOwner({
        houseOwnerName: req.body.username,
        telephone: req.body.phone,
        email: req.body.email,
        pass: req.body.password
      });
      user.save(err => {
        if (err) return res.status(500).send(err.message);
        return res.json({
          success: true,
          user
        });
      });
    } else {
      return res.json({
        success: false,
        message: 'Duplicate username'
      });
    } // return res.json({ success: true, user })

  });
});
module.exports = router;