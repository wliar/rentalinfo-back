let Information = require('../models/RentalInformation');

let HouseOwner = require('../models/HouseOwner');

let express = require('express');

let router = express.Router();

router.findAllInfo = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  Information.find(function (err, RentalInformation) {
    if (err) res.send(err);
    res.send(RentalInformation);
  });
};

router.findAllOwner = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  HouseOwner.find(function (err, HouseOwnerInformation) {
    if (err) res.send(err);
    res.send(HouseOwnerInformation);
  });
};

router.findOneId = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  console.log(req.params);
  Information.findOne({
    "_id": req.params.id
  }, function (err, donation) {
    if (err) res.json({
      message: 'Information NOT FOUND!',
      errmsg: err
    });else res.send(JSON.stringify(donation));
  });
};

router.findOneHouseOwnerName = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  Information.findOne({
    "houseOwner": req.params.HouseOwnerName
  }, function (err, information) {
    if (err) res.json({
      message: 'Information NOT Found!',
      errmsg: err
    });else res.send(JSON.stringify(information));
  });
};

router.addInfo = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  var information = new Information();
  information.number = req.body.number;
  information.price = req.body.price;
  information.address = req.body.address;
  information.houseOwner = req.body.houseOwner;
  information.save(function (err) {
    if (err) res.json({
      message: 'Added Failed',
      errmsg: err
    });else res.json({
      message: 'Added Successfully!',
      data: information
    });
  });
};

router.modifyUpHouses = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  Information.findById(req.params.id, function (err, ri) {
    if (err) {
      res.json({
        message: 'Information NOT Found',
        errmsg: err
      });
    } else {
      ri.number = req.body.number;
      ri.price = req.body.price;
      ri.address = req.body.address;
      ri.houseOwner = req.body.houseOwner;
      ri.save(function (err) {
        if (err) {
          res.send(err);
        } else {
          res.send({
            message: 'Modified Successfully!'
          });
        }
      });
    }
  });
};

router.updateHouseOwner = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  HouseOwner.findOneAndUpdate({
    houseOwnerName: req.params.HouseOwnerName
  }, {
    $set: {
      telephone: req.body.telephone,
      email: req.body.email
    }
  }, function (err) {
    if (err) {
      res.json({
        success: false,
        message: 'Information NOT Found',
        errmsg: err
      });
    } else {
      res.json({
        success: true,
        message: 'Updated Successfully!'
      });
    }
  });
};

router.deleteOwner = (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  HouseOwner.findOneAndDelete({
    "houseOwnerName": req.params.HouseOwnerName
  }, function (err, owner) {
    if (err) res.json({
      message: 'Information NOT DELETED!',
      errmsg: err
    });

    if (owner) {
      res.json({
        message: 'Deleted Successfully!',
        data: HouseOwner
      });
    } else {
      res.json({
        message: 'Information NOT DELETED!',
        errmsg: err
      });
    }
  });
};

router.deleteInfo = (req, res) => {
  Information.findByIdAndRemove(req.params.id, function (err) {
    if (err) res.json({
      message: 'Information NOT DELETED!',
      errmsg: err
    });else res.json({
      message: 'Deleted Successfully!',
      data: Information
    });
  });
};

module.exports = router;