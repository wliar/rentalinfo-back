let mongoose = require('mongoose');

let RentalInformation = new mongoose.Schema({
        number: String,
        price: String,
        address: String,
        houseOwner: String,
    }, {versionKey: false}
);

module.exports = mongoose.model('RentalInformation', RentalInformation);